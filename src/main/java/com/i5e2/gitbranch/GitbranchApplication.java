package com.i5e2.gitbranch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitbranchApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitbranchApplication.class, args);
    }

}
