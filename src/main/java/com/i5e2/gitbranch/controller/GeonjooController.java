package com.i5e2.gitbranch.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/name")
public class GeonjooController {
    @GetMapping("/geonjoo")
    public String geonjooNameController() {
        return "geonjoo";
    }

}
