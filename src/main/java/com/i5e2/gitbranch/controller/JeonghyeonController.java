package com.i5e2.gitbranch.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JeonghyeonController {

    @GetMapping("name/jeonghyeon")
    public String getName() {
        return "KIMJEONGHYEON";
    }
}
