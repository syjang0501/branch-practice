package com.i5e2.gitbranch.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class YejinController {
    @GetMapping("name/yejin")
    public String hello(){
        return "YEJIN";
    }
}
